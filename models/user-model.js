const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//TODO :: Modifier le nom u string google id par provicderID et repercuter ce changement partout
const userSchema = new Schema ({
    username: String,
    providerId: String,
    thumbnail: String
});

const User = mongoose.model('user', userSchema);

module.exports = User;