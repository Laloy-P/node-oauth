const router = require('express').Router();

const authCheck = (req,res,next) => {
    if(!req.user){
        //on redirige l'utilisateur qui n'est pas loggedIn
        res.redirect('/auth/login');
    } else {
        next();
    }
}

router.get('/', authCheck, (req,res) => {
    res.render('profile', {user: req.user});
});

module.exports = router;