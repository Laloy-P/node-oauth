const router = require ('express').Router();
const passport = require ('passport');
const providersController = require('../controllers/providersController');


router.get('/login', providersController.login);
router.get('/logout', providersController.logout);
router.get('/google', providersController.google_get);
//Callback route pour permettre a google de se rediriger vers nous
router.get('/google/redirect', passport.authenticate('google'), providersController.google_redirect_get);
router.get('/gitlab', providersController.gitlab_get);
//Gitlab callback
router.get('/gitlab/redirect', passport.authenticate('gitlab'), providersController.gitlab_redirect_get);

module.exports = router; 