const express = require('express');
const authRoutes = require('./routes/auth-routes');
const profileRoutes = require('./routes/profile-routes');
const passportSetup = require ('./config/passport-setup');
const mongoose = require ('mongoose');
require('dotenv').config();
const cookieSession = require('cookie-session');
const passport = require('passport');

//1 journee en milisecondes -H----M---S-----mS    
const dureeValiditeCookie = 24 * 60 *60 * 1000

const app = express();

//Set up view engine
app.set('view engine', 'ejs');

//Chiffre un cookie, il sera valide pour une duree de 1 jour
app.use(cookieSession({
    maxAge: dureeValiditeCookie,
    keys:[process.env.COOKIE_KEY]
}));

//Initialise passport
app.use(passport.initialize());
app.use(passport.session());

//Connect to mongo db
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true  },() => {
    console.log('connecter a Mongo DB.');
});

//Set up routes
app.use('/auth', authRoutes);
app.use('/profile', profileRoutes);

//Create home route
app.get('/', (req, res) => {
    res.render('home', {user: req.user});
});

//Demarrage du serveur.
app.listen(3000, () => {
    console.log('serveur up on 3000');
});
