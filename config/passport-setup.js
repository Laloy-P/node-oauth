const passport = require('passport');
const GoogleStrategy = require ('passport-google-oauth20');
const GitLabStrategy = require('passport-gitlab2');
const User = require('../models/user-model');
require('dotenv').config();

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id).then((user) => {
        done(null, user);
    });
});

passport.use(
    new GoogleStrategy ({
        //Options de la strategie
        callbackURL:'/auth/google/redirect',
        clientID: process.env.GOOGLE_OAUTH_CLIENT_ID,
        clientSecret: process.env.GOOGLE_OAUTH_CLIENT_SECRET

    }, (accessToken, refreshToken, profile, done) => { //callback function triggerd lorsque l'on est rediriger sur /google/redirect
        // verif si l'utilisateur existe dans la bdd
        User.findOne({providerId: profile.id}).then((currentUser) => {
            // verif si l'utilisateur existe deja dans la bdd
            if (currentUser) {
                // l'utilisateur existe deja
                console.log('user is :', currentUser);
                done(null, currentUser);
            } else {
                // sinon on le creer et on l'insere en bdd
                new User ({
                    username: profile.displayName,
                    providerId: profile.id,
                    thumbnail: profile._json.picture
                }).save().then((newUser) => {
                    console.log('Nouvel Utilisateur creer : ', newUser);
                    done(null, newUser);
                });
            }
        });
    })
);

passport.use(
    new GitLabStrategy (
        {
        //Options de la strategie
        callbackURL:'/auth/gitlab/redirect',
        clientID: process.env.GITLAB_OAUTH_CLIENT_ID,
        clientSecret: process.env.GITLAB_OAUTH_CLIENT_SECRET

        }, (accessToken,refreshToken, profile, done) => {
            console.log(profile);
            User.findOne({providerId: profile.id}).then((currentUser) => {
                // verif si l'utilisateur existe deja dans la bdd
                if (currentUser) {
                    // l'utilisateur existe deja
                    console.log('user is :', currentUser);
                    done(null, currentUser);
                } else {
                    // sinon on le creer et on l'insere en bdd
                    new User ({
                        username: profile.displayName,
                        providerId: profile.id,
                        thumbnail: profile.avatarUrl
                    }).save().then((newUser) => {
                        console.log('Nouvel Utilisateur creer : ', newUser);
                        done(null, newUser);
                    });
                }
            });

        }
    )
);