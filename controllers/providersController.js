const passport = require ('passport');

module.exports.login = (req, res) => {
    res.render('login', {user: req.user});
};

module.exports.logout = (req, res) => {
    // handle with passport
     req.logout();
     res.redirect('/')
 };

module.exports.google_get = passport.authenticate('google', {
    scope: ['profile']// ce que l'on veut reccuperer comme info depuis google.
});

module.exports.google_redirect_get =  (req, res) => {
    res.redirect('/profile/');
};

module.exports.gitlab_get = passport.authenticate('gitlab', {
    scope: ['api']// ce que l'on veut reccuperer comme info depuis gitlab.
});

module.exports.gitlab_redirect_get =  (req, res) => {
    res.redirect('/profile/');
};