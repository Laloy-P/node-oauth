# Node OAuth

## Utilisation de OAuth 2.0

Utilisation des technos suivante : 

    - Node
    - Express
    - MongoDb (Atlas)
    - Mongoose
    - OAuth

## A propos

Prise en main de l'authentifiacation avec OATH. 

### Lancer le projet

    1- Vous devez avoir configuré une bdd via [MongDB Atlas](https://www.mongodb.com/cloud/atlas)
    2- Vous devez avoir configurer une application via Google Developpeur Console et GitLab
    3- Remplacer les informations de connexion dans le fichier .env-sample et le renomer en .env
    4- run : npm install
    5- run : node app.js OU: nodemon app.js

#### Configurer GitLab

    Se rendre dans settings/application 
    Ajouter un nom + l'uri de callBack (ici :http://localhost:3000/auth/gitlab/redirect)
    Selectionner la scope API
    Puis "Save Application"

#### Configurer Google

    En gros se rendre dans google cloud plateforme creer un projet, 
    ensuite se rendre dans Identifiants et ajouter ID clients OAuth 2.0. 
    Configurer les URI ici   http://localhost:3000/auth/google URI de callback http://localhost:3000/auth/google/redirect
    ajouter people api et dans sa configurationajouter les credential OAuth precedement creer.

    









